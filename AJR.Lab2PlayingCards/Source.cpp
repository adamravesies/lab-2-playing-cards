// Adam Ravesies
// Lab 2: Playing Cards

#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{
	TWO = 2, 
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE= 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};

enum Suit
{
	CLUBS,
	DIAMONDS,
	HEARTS, 
	SPADES
};

struct Card
{
	Rank rank;
	Suit suit;
};


int main()
{
	(void)_getch();
	return 0;
}